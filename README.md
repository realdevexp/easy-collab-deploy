# EasyCollab Docker Version

### Prerequisites (For running locally)

-   Docker must be installed in your local system. If not visit: [Docker Guide](https://docs.docker.com/get-started/)

-   Run `bash clone.sh`. This will clone frontend and backend files.

-   Rename `.env.example` to `.env` and put your new credentials for Postgres database running in docker

-   Rename `.env.example` to `.env` and update `DATABASE_URL` env variable according to your new postgres credentials and other variables in `server/.env`. your DB_HOST will be postgres container name which is `postgres` in this case. (Check `docker-compose.yml` file)

### Setup

-   Run `docker compose build` to build new container. (This will take sometime)
-   Run `docker compose up` to run the newly created container.
-   Launch browser: `http://localhost/login` . You should see login page.
